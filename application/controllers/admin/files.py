#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os, mimetypes
mimetypes.add_type('video/3gpp', '3gp')

from application.models import files
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.ext.webapp.util import run_wsgi_app

File = files.File
FileCategory = files.FileCategory
TableVars = ["delete","set_public","set_private"]


class MainPage(webapp.RequestHandler):
	def get_data(self):
		return File.all().fetch(100)

	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		tools.render(self, gp, 'table', {'files': self.get_data()})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([unicode(x) for x in File.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in list(self.request.POST)])).split(",")
		while '' in keys:
			keys.remove('')
			
		while keys.count('all_entities'):
			keys.remove('all_entities')
		while keys.count('all_entities_'):
			keys.remove('all_entities_')
		
		if self.request.get('delete', default_value=False):
			keys.remove('delete')
			db.delete(keys)
		elif self.request.get('delete_categories', default_value=False):
			keys.remove('delete_categories')
			contents = File.all()
			for key in keys:
				contents.filter('categories_keys IN', [key])
				
			for c in contents.fetch(1000):
				for key in keys:
					c.categories_keys.remove(key)
					c.save()
					
			db.delete(keys)
		else:
			for key in keys:
				if key not in TableVars:
					try:
						file = db.get(db.Key(key))
						if file:
							#if self.request.get('delete', default_value=False):
								#file.delete()
								#file.public = -2
							if self.request.get('set_public', default_value=False):
								file.public = 1
							elif self.request.get('set_private', default_value=False):
								file.public = 0
							elif self.request.get('set_draft', default_value=False):
								file.public = -1
							file.save()
					except Exception, e:
						gp.error = e
						gp.actionstatus = -1
						
		tools.render(self, gp, 'table', {'files': self.get_data()})

		
class Add(webapp.RequestHandler):	
	def get_data(self, key):
			return File.get(db.Key(key))

	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)

		if gp.action == "update":
			gp.page.title = "Update file details"
			file = self.get_data(tools.get_url_var(gp.action))
		else:
			gp.page.title = "Upload a file"
			file = {}

		from application.gpress.mimetypes import mimetypes as m
		tools.render(self, gp, 'form', {'file': file, 'mimetypes': m})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True
		
		if self.request.get('key', default_value=False):
			gp.page.title = "Update file details"
			""" Update entry """
			try:
				file = db.get(db.Key(self.request.get('key')))
				# Update
				if file.protection <= 1 or file.user == gp.user_instance or gp.user.is_developer():
					file.update(self.request.POST)
					# Save
					file.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
					
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
		else:
			gp.page.title = "Add new file"
			""" Add file to db """
			file = File()
			file.update(self.request.POST)
			if self.request.get('permalink', default_value=False):
				file.permalink = self.request.get('permalink')
			else: 
				key_name = self.request.get('title', default_value=False)
				if not key_name:
					key_name = self.request.POST.get('stream').filename
				file.permalink = tools.key_name(key_name , token=True)

				
			if self.request.get('stream', default_value=False):
				file.stream=db.Blob(self.request.POST.get('stream').file.read())
				file.filename = self.request.POST.get('stream').filename
				file.mimetype = mimetypes.guess_type(file.filename)[0]
				file.size = len(file.stream)
			elif gp.has_key("newLanguage") and gp.newLanguage:
				file.main = File.all().filter('permalink =', file.permalink).filter('main !=', None).fetch(1)[0]

			file.put()
			self.redirect("/gp-admin/files/update/%s" % file.key(), False)
			return

		from application.gpress.mimetypes import mimetypes as m
		tools.render(self, gp, 'form', {'file': file, 'mimetypes': m})
			

class UpdateSharing(webapp.RequestHandler):
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		gp.actionstatus = True
		file = []
		if self.request.get('key', default_value=False):
			gp.page.title = "Update file"
			try:
				file = db.get(db.Key(self.request.get('key')))
				# Update
				if file.protection <= 1 or file.user == gp.user_instance or gp.user.is_developer():
					file.update_sharing(self.request.POST)
					# Save
					file.save()
				else:
					gp.error = {'message':'Forbidden'}
					gp.actionstatus = -1
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
				
			tools.render(self, gp, 'form', {'file': file})


class Delete(webapp.RequestHandler):
	def get(self):
		gpress.Registry(self, gpress.CONTRIBUTOR)
		protected = (",".join([str(x) for x in File.all(keys_only=True).filter('protection >=', 1).fetch(1000)])).split(",")
		keys = (",".join([x if x not in protected else '' for x in tools.get_requested_object_list(3)])).split(",")
		
		while '' in keys:
			keys.remove('')
		if 'delete' in keys:
			keys.remove('delete')
		if 'all_entities' in keys:
			keys.remove('all_entities')	
		
		db.delete(keys)
		
		self.redirect(os.environ["HTTP_REFERER"], False)


application = webapp.WSGIApplication([('/gp-admin/files/add', Add),
																													('/gp-admin/files/update/.*/sharing', UpdateSharing),
																													('/gp-admin/files/update/.*', Add),
																													('/gp-admin/files/delete/.*', Delete),
																													('/gp-admin/files.*', MainPage)],
																													gpress.Debug)

def main():
	run_wsgi_app(application)

if __name__ == "__main__":
	main()