#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import os

from application.models import users as gusers
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp, db
from google.appengine.api import users
from google.appengine.ext.webapp.util import run_wsgi_app

User = gusers.User

class MainPage(webapp.RequestHandler):
	def get_data(self, rank):
		return User.all().filter('rank <=', rank).order('-rank').fetch(100, tools.get_offset())

	def get(self):
		gp = gpress.Registry(self, gpress.OPERATOR)
		tools.render(self, gp, 'table', {'users': self.get_data(gp.user.rank)})
		
	def post(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		keys = list(self.request.POST)
		while keys.count('all_entities'):
			keys.remove('all_entities')

		if self.request.get('delete', default_value=False):
			keys.remove('delete')
			db.delete(keys)
			
		tools.render(self, gp, 'table', {'users': self.get_data(gp.user.rank)})
		
			
class Add(webapp.RequestHandler):
	def get_data(self, key):
		if key == "me":
			return User.all().filter('email =', users.get_current_user().email()).fetch(1)[0]
		else:
			return User.get(db.Key(key))
		
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		
		if gp.action == "update":
			gp.page.title = "Update user details"
			user = self.get_data(tools.get_url_var(gp.action))
		else:
			gp.page.title = "Add a new user"
			user = {}
		
		tools.render(self, gp, 'form', {'user': user})
		
	def post(self):
		if self.request.get('key', default_value=False):
			gp = gpress.Registry(self, gpress.USER)
			gp.actionstatus = True
			try:
				user = User.get(self.request.get('key'))
				#if user.email != users.get_current_user().email() and not gp.user.is_operator():
					#self.redirect('/gp-admin/errors/forbidden', True)
					#return
				
				user.update(self.request.POST, gp.user.rank)
				user.save()		
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
		else:
			gp = gpress.Registry(self, gpress.OPERATOR)
			gp.actionstatus = True
			try:
				user = User(email=self.request.get('email'))
				user.update(self.request.POST, gp.user.rank)
				user.put()
				if not gp.site.is_ajax_request:
					self.redirect("/gp-admin/users/update/%s" % user.key(), False)
					return
				
			except Exception, e:
				gp.error = e
				gp.actionstatus = -1
						
		gp.page.title = "Update user details"
		tools.render(self, gp, 'form', {'user': user})

		
class AddMe(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry(self, gpress.GOOGLEUSER)
		
		if gp.site.register_open:
			if users.get_current_user():
				user = User.all().filter('email =', users.get_current_user().email())
				if not user.count(1):
					user = User(email=users.get_current_user().email())
					user.name = users.get_current_user().nickname()
					if users.is_current_user_admin(): 
						user.rank = gpress.DEVELOPER
					else:
						user.rank = gpress.USER
						
					user.put()
					self.redirect("/gp-admin/users/update/%s" % user.key(), False)
		
		
class Delete(webapp.RequestHandler):
	def get(self):
		gpress.Registry(self, gpress.ADMINISTRATOR)
		
		keys = tools.get_requested_object_list(3)
		if 'delete' in keys:
			keys.remove('delete')
		if 'all_entities' in keys:
			keys.remove('all_entities')
		db.delete(keys)
		
		self.redirect(os.environ["HTTP_REFERER"], False)


application = webapp.WSGIApplication([('/gp-admin/users/add', Add),
									 										                    ('/gp-admin/users/add/me', AddMe),
									 										                    ('/gp-admin/users/update/.*', Add),
									 										                    ('/gp-admin/users/delete/.*', Delete),
									 										                    ('/gp-admin/users.*', MainPage)],
									 										                    gpress.Debug)

def main():
	run_wsgi_app(application)
  
if __name__ == "__main__":
	main()