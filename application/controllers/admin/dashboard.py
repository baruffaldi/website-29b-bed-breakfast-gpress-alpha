#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.models import settings
from application.gpress import tools
from application import gpress

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

Settings = settings.Settings


class MainPage(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry(self, gpress.CONTRIBUTOR)
		
		tools.render(self, gp, 'table', {'settings': Settings.all().fetch(1000)})


application = webapp.WSGIApplication([('/gp-admin.*', MainPage)],
									  									                    gpress.Debug)

def main():
	run_wsgi_app(application)
  
if __name__ == "__main__":
	main()