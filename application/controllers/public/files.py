#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import logging

from urllib import urlencode

from application.models import files 
from application.gpress import tools
from application import gpress 

from google.appengine.ext import webapp
from google.appengine.api import memcache
from google.appengine.ext.webapp.util import run_wsgi_app

File = files.File

class Main(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry(self)
		tools.parseHandler(self, gp)
		filez = File.all().filter('publish_status >=', 1)
		filez.order('-publish_status')
		filez.order('-counter')
		filez.fetch(int(gp.site.files_limit), tools.get_offset())
				
		tools.render(self, gp, 'files', {'files': filez})
				
				
class Preview(webapp.RequestHandler):
	def get(self):
		gp = gpress.Registry(self)
		tools.parseHandler(self, gp)
		
		try: file = File.get(tools.get_url_var('files'))
		except: 
			file_key = File.all(keys_only=True)
			file_key.filter('permalink =', tools.get_url_var('files'))
			if not gp.user.is_contributor():
				file_key.filter('publish_status >=', 1)
						
			if file_key.count(1):
				file = File.get(file_key.fetch(1)[0])
			else:
				self.redirect('/errors/document-not-found?%s' %
							 urlencode({'ref': self.request.uri}), True)
				return

		gp.page.process_page(file)
		tools.render(self, gp, 'file', {'file': file})
				
				
class Download(webapp.RequestHandler):
	def get_data(self):
		gp = gpress.Registry(self)
		key = tools.get_url_var('files')
		data = memcache.get("file%s" % key)
		if data is not None:
			return data
		
		else:
			file = File.all(keys_only=True)
			file.filter('permalink =', key)
			if not gp.user.is_contributor():
				file.filter('publish_status =', True)
					
			if file.count(1):
				data = memcache.get("file%s" % file.fetch(1)[0])
				if data is not None:
					return data
			
			file = File.all()
			file.filter('permalink =', key)
			if not gp.user.is_contributor:
				file.filter('publish_status =', True)
			
			if file.count(1):
				file = file.fetch(1)[0]
				if not memcache.add("file%s" % key, file, 10):
					logging.error("Memcache set file %s failed." % key)
				else:
					memcache.replace("file%s" % key, file)
					
				if not self.request.get('disable_counter', default_value=False):
					file.counter += 1
					file.save()
					
				return file

		return False
			
	def get(self):
		file = self.get_data()
		tools.parseHandler(self, gp)

		if file:
			self.response.headers['X-Powered-By'] = 'GPress (+http://www.baruffaldi.info)'
			self.response.headers["Content-Type"] = file.mimetype + "; charset=\"utf-8\""
			self.response.headers["Content-Length"] = file.size
			if self.request.get('force', default_value=False):
					self.response.headers["Content-Disposition"] = "attachment; filename=\""+str(file.filename)+"\""
			else: self.response.headers["Content-Disposition"] = "inline; filename=\""+str(file.filename)+"\""

			self.response.headers["Expires"] = "Wed, 27 May 2099 16:00:00 GMT"
			
			self.response.headers["Content-Transfer-Encoding"] = "binary"
			self.response.out.write(file.stream)
			return

		self.redirect('/errors/document-not-found?' +
					 urlencode({'ref': self.request.uri}), True)


application = webapp.WSGIApplication([('/files', Main),
																			('/files/', Main),
																			('/files/.*/download', Download),
																			('/files/.*', Preview)],
																		 gpress.Debug)

def main():
		run_wsgi_app(application)

if __name__ == "__main__":
		main()