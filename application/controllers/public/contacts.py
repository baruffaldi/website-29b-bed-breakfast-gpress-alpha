#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from urllib import urlencode

from application.models import contacts 
from application.gpress import tools
from application import gpress 

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

Contact = contacts.Contact

class Main(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    contactz = Contact.all()
    contactz.fetch(int(gp.site.contacts_limit), tools.get_offset())
        
    tools.render(self, gp, 'contacts', {'contacts': contactz})


class GuestbookSign(webapp.RequestHandler):
  def get(self):
    return
  
class Preview(webapp.RequestHandler):
  def get(self):
    # SE SU SETTINGS DICE DI NON MOSTRARE LA PREVIEW CONTROLLARE
    # LA VARIABILE PREVIEW ALTRIMENTI REDIRECT DIRETTO
    gp = gpress.Registry()
    
    try: contact = Contact.get(tools.get_url_var('contacts'))
    except:
      ""
          
    if contact.count(1):
      gp.page.process_page(contact)
      tools.render(self, gp, 'contact', {'contact': contact})
      return
    
    self.redirect('/errors/document-not-found?%s' %
           urlencode({'ref': self.request.uri}), True)
    return
        


application = webapp.WSGIApplication([('/contacts', Main),
                                                          ('/contacts/', Main),
                                                          ('/contacts/guestbook/sign', GuestbookSign),
                                                          ('/contacts.*', Preview)],
                                                          gpress.Debug)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()