#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.models import news, posts, pages, media, files, links
from application import gpress
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

# INSERIRE CONTROLLI UTENTE ( PER AGGIUNGERE QUELLI PRIVATI E QUELLI CONDIVISI DA ALTRI )
class News(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = news.News.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('main =', None)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'application/json'
    self.response.headers["Content-Disposition"] = "inline; filename=\"news.json\""
    self.response.out.write(template.render('../../views/feeds/gpress.json', {'entries': data}))
    
class Posts(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = posts.Post.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('main =', None)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'application/json'
    self.response.headers["Content-Disposition"] = "inline; filename=\"pages.json\""
    self.response.out.write(template.render('../../views/feeds/gpress.json', {'entries': data}))

class Pages(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = pages.Page.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('main =', None)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'application/json'
    self.response.headers["Content-Disposition"] = "inline; filename=\"pages.json\""
    self.response.out.write(template.render('../../views/feeds/gpress.json', {'entries': data}))
      
class Media(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = media.Media.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'application/json'
    self.response.headers["Content-Disposition"] = "inline; filename=\"media.json\""
    self.response.out.write(template.render('../../views/feeds/gpress.json', {'entries': data}))
      
class Files(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = files.File.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'application/json'
    self.response.headers["Content-Disposition"] = "inline; filename=\"files.json\""
    self.response.out.write(template.render('../../views/feeds/gpress.json', {'entries': data}))

class Links(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = links.Link.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'application/json'
    self.response.headers["Content-Disposition"] = "inline; filename=\"links.json\""
    self.response.out.write(template.render('../../views/feeds/gpress.json', {'entries': data}))

application = webapp.WSGIApplication([('/json/news.*', News),
                                                          ('/json/posts.*', Posts),
                                                          ('/json/pages.*', Pages),
                                                          ('/json/media.*', Media),
                                                          ('/json/files.*', Files),
                                                          ('/json/links.*', Links)],
                                                           True)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()