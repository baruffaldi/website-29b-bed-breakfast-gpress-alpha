#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from urllib import urlencode

from application.models import links 
from application.gpress import tools
from application import gpress 

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

Link = links.Link
LinkComment = links.LinkComment

class Main(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    linkz = Link.all().filter('publish_status >=', 1)
        
    #linkz.order('-counter')
    linkz.fetch(int(gp.site.links_limit), tools.get_offset())
        
    tools.render(self, gp, 'links', {'links': linkz})


class Preview(webapp.RequestHandler):
  def get(self):
    # SE SU SETTINGS DICE DI NON MOSTRARE LA PREVIEW CONTROLLARE
    # LA VARIABILE PREVIEW ALTRIMENTI REDIRECT DIRETTO
    gp = gpress.Registry()
    tools.parseHandler(self, gp)
    
    try: link = Link.get(tools.get_url_var('links'))
    except: 
      link_key = Link.all(keys_only=True)
      link_key.filter('permalink =', tools.get_url_var('links'))
      if not gp.user.is_contributor():
        link_key.filter('publish_status >=', 1)
          
      if link_key.count(1):
        link = Link.get(link_key.fetch(1)[0])
      else:
        self.redirect('/errors/document-not-found?%s' %
               urlencode({'ref': self.request.uri}), True)
        return
        
    gp.page.process_page(link)
    tools.render(self, gp, 'link', {'link': link})


class Redirect(webapp.RequestHandler):
  def get(self):
    try:
      link = Link.all().filter('permalink', tools.get_url_var('links')).fetch(1)[0]
      link.counter += 1
      link.save()
      self.redirect(link.url, False)
    except: 
      self.redirect('/errors/document-not-found?%s' %
             urlencode({'ref': self.request.uri}), True)


application = webapp.WSGIApplication([('/links', Main),
                                                          ('/links/', Main),
                                                          ('/links/.*/redirect', Redirect),
                                                          ('/links.*', Preview)],
                                                          gpress.Debug)

def main():
  run_wsgi_app(application)

if __name__ == "__main__":
  main()