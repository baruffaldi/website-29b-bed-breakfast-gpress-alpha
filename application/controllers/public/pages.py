#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from urllib import urlencode

from application.models import pages, posts, links, media, files, contacts, news
from application.gpress import tools
from application import gpress 

from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

Page = pages.Page
PageComment = pages.PageComment


class Main(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    
    pagez = Page.all().filter('publish_status >=', 1)
        
    pagez.fetch(int(gp.site.pages_limit), tools.get_offset())
    tools.render(self, gp, 'pages', {'pages': pagez})


class Get(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    tools.parseHandler(self, gp)
    
    page = Page.all()
    if not gp.user.is_contributor():
      page.filter('publish_status >=', 1)
    page.filter('permalink =', tools.get_requested_object_list(1,1)[0])
    page.filter('language =', gp.user.language)

    if page.count(1):
      p = page.fetch(1)[0]
    else:
      page = Page.all()
      if not gp.user.is_contributor():
        page.filter('publish_status >=', 1)
      page.filter('permalink =', tools.get_requested_object_list(1,1)[0])
      page.filter('language =', gp.site.language)
      if page.count(1):
        p = page.fetch(1)[0]
      else:
        page = Page.all()
        if not gp.user.is_contributor():
          page.filter('publish_status >=', 1)
        page.filter('permalink =', tools.get_requested_object_list(1,1)[0])
        if page.count(1):
          p = page.fetch(1)[0]
        else:
          self.redirect('/errors/document-not-found?%s' %
                 urlencode({'ref': self.request.uri}), True)
          return
    tools.render(self, gp, 'page', {'page': p})


application = webapp.WSGIApplication([('/pages.*', Main),
                                                          ('/.*', Get)],
                                                          gpress.Debug)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()