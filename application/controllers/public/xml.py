#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from application.models import news, posts, pages, media, files, links
from application import gpress

from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

class News(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = news.News.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('main =', None)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'text/xml'
    self.response.headers["Content-Disposition"] = "inline; filename=\"news.xml\""
    self.response.out.write(template.render('../../views/feeds/gpress.xml', {'entries': data}))
    

class Posts(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = posts.Post.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('feed_shared =', True)
    data.filter('main_post =', None)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'text/xml'
    self.response.headers["Content-Disposition"] = "inline; filename=\"posts.xml\""
    self.response.out.write(template.render('../../views/feeds/gpress.xml', {'entries': data, 'gpress': gpress.Registry()}))

class Pages(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = pages.Page.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('feed_shared =', True)
    data.filter('main_page =', None)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'text/xml'
    self.response.headers["Content-Disposition"] = "inline; filename=\"pages.xml\""
    self.response.out.write(template.render('../../views/feeds/gpress.xml', {'entries': data, 'gpress': gpress.Registry()}))
      
class Media(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = media.Media.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('feed_shared =', True)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'text/xml'
    self.response.headers["Content-Disposition"] = "inline; filename=\"media.xml\""
    self.response.out.write(template.render('../../views/feeds/gpress.xml', {'entries': data, 'gpress': gpress.Registry()}))
      
class Files(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = files.File.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('feed_shared =', True)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'text/xml'
    self.response.headers["Content-Disposition"] = "inline; filename=\"files.xml\""
    self.response.out.write(template.render('../../views/feeds/gpress.xml', {'entries': data, 'gpress': gpress.Registry()}))

class Links(webapp.RequestHandler):
  def get(self):
    gp = gpress.Registry(self, gpress.ANONYMOUS)
    data = links.Link.all()
    data.filter('publish_status >=', 0 if gp.user.is_contributor() else 1)
    data.filter('feed_shared =', True)
    data.fetch(1000)
    self.response.headers["Content-Type"] = 'text/xml'
    self.response.headers["Content-Disposition"] = "inline; filename=\"links.xml\""
    self.response.out.write(template.render('../../views/feeds/gpress.xml', {'entries': data, 'gpress': gpress.Registry()}))

application = webapp.WSGIApplication([('/xml/posts.*', Posts),
                                                          ('/xml/news.*', News),
                                                          ('/xml/pages.*', Pages),
                                                          ('/xml/media.*', Media),
                                                          ('/xml/files.*', Files),
                                                          ('/xml/links.*', Links)],
                                                           gpress.Debug)

def main():
  run_wsgi_app(application)
  
if __name__ == "__main__":
  main()