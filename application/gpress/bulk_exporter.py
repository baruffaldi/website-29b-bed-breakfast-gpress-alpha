from google.appengine.tools import bulkloader

class Settings(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Settings',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('field', str, None),
                                      ('value', str, None)
                                     ])

class News(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'News',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('publish_date', str, None),
                                      ('publish_user_key', str, None),
                                      ('permalink', str, None),
                                      ('title', str, None),
                                      ('body', str, None),
                                      ('tags', list, None),
                                      ('categories_keys', list, None),
                                      ('publish_status', str, None),
                                      ('feed_shared', int, None),
                                      ('sitemap_shared', int, None),
                                      ('sitemap_priority', float, None),
                                      ('sitemap_freq', str, None),
                                      ('protection', int, None),
                                      ('contributors_keys', list, None),
                                      ('main_page', str, None),
                                      ('language', str, None),
                                      ('publish_status', int, None),
                                      ('attachments_keys', list, None)
                                     ])

class Page(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Page',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('publish_date', str, None),
                                      ('publish_user_key', str, None),
                                      ('permalink', str, None),
                                      ('title', str, None),
                                      ('body', str, None),
                                      ('tags', list, None),
                                      ('categories_keys', list, None),
                                      ('publish_status', str, None),
                                      ('feed_shared', int, None),
                                      ('sitemap_shared', int, None),
                                      ('sitemap_priority', float, None),
                                      ('sitemap_freq', str, None),
                                      ('protection', int, None),
                                      ('contributors_keys', list, None),
                                      ('main_page', str, None),
                                      ('language', str, None),
                                      ('publish_status', int, None),
                                      ('attachments_keys', list, None)
                                     ])

class PageCategory(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'PageCategory',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('title', str, None)
                                     ])

class PageComment(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'PageComment',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('content', str, None),
                                      ('body', str, None),
                                      ('status', int, None)
                                     ])

class Post(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Post',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('publish_date', str, None),
                                      ('publish_user_key', str, None),
                                      ('permalink', str, None),
                                      ('title', str, None),
                                      ('body', str, None),
                                      ('tags', list, None),
                                      ('categories_keys', list, None),
                                      ('publish_status', str, None),
                                      ('feed_shared', int, None),
                                      ('sitemap_shared', int, None),
                                      ('sitemap_priority', float, None),
                                      ('sitemap_freq', str, None),
                                      ('protection', int, None),
                                      ('contributors_keys', list, None),
                                      ('main_page', str, None),
                                      ('language', str, None),
                                      ('publish_status', int, None),
                                      ('attachments_keys', list, None)
                                     ])

class PostCategory(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'PostCategory',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('title', str, None)
                                     ])

class PostComment(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'PostComment',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('content', str, None),
                                      ('body', str, None),
                                      ('status', int, None)
                                     ])

class Media(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Media',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('publish_date', str, None),
                                      ('publish_user_key', str, None),
                                      ('permalink', str, None),
                                      ('title', str, None),
                                      ('body', str, None),
                                      ('tags', list, None),
                                      ('albums_keys', list, None),
                                      ('publish_status', str, None),
                                      ('feed_shared', int, None),
                                      ('sitemap_shared', int, None),
                                      ('sitemap_priority', float, None),
                                      ('sitemap_freq', str, None),
                                      ('protection', int, None),
                                      ('contributors_keys', list, None),
                                      ('filename', str, None),
                                      ('mimetype', str, None),
                                      ('size', int, None),
                                      ('stream', unicode, None),
                                      ('counter', int, None)
                                     ])

class MediaAlbum(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'MediaAlbum',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('title', str, None)
                                     ])

class MediaComment(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'MediaComment',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('content', str, None),
                                      ('body', str, None),
                                      ('status', int, None)
                                     ])

class File(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Page',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('publish_date', str, None),
                                      ('publish_user_key', str, None),
                                      ('permalink', str, None),
                                      ('title', str, None),
                                      ('body', str, None),
                                      ('tags', list, None),
                                      ('categories_keys', list, None),
                                      ('publish_status', str, None),
                                      ('feed_shared', int, None),
                                      ('sitemap_shared', int, None),
                                      ('sitemap_priority', float, None),
                                      ('sitemap_freq', str, None),
                                      ('protection', int, None),
                                      ('contributors_keys', list, None),
                                      ('filename', str, None),
                                      ('mimetype', str, None),
                                      ('size', int, None),
                                      ('stream', unicode, None),
                                      ('counter', int, None)
                                     ])

class FileCategory(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'FileCategory',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('title', str, None)
                                     ])

class FileComment(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'FileComment',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('content', str, None),
                                      ('body', str, None),
                                      ('status', int, None)
                                     ])

class Link(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Link',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('publish_date', str, None),
                                      ('publish_user_key', str, None),
                                      ('permalink', str, None),
                                      ('title', str, None),
                                      ('body', str, None),
                                      ('tags', list, None),
                                      ('categories_keys', list, None),
                                      ('publish_status', str, None),
                                      ('feed_shared', int, None),
                                      ('sitemap_shared', int, None),
                                      ('sitemap_priority', float, None),
                                      ('sitemap_freq', str, None),
                                      ('protection', int, None),
                                      ('contributors_keys', list, None),
                                      ('url', str, None),
                                      ('image', str, None),
                                      ('counter', int, None),
                                      ('favicon_key', str, None)
                                     ])

class LinkCategory(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'LinkCategory',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('title', str, None)
                                     ])

class LinkComment(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'LinkComment',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('content', str, None),
                                      ('body', str, None),
                                      ('status', int, None)
                                     ])

class Contact(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Page',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('name', str, None),
                                      ('address', str, None),
                                      ('location', str, None),
                                      ('cap', str, None),
                                      ('body', str, None),
                                      ('state', str, None)
                                     ])

class User(bulkloader.Exporter):
    def __init__(self):
        bulkloader.Exporter.__init__(self, 'Page',
                                     [('user', str, None),
                                      ('date', str, None),
                                      ('update_date', str, None),
                                      ('update_user', str, None),
                                      ('name', str, None),
                                      ('email', str, None),
                                      ('rank', int, None)
                                     ])

exporters = [
             Settings,
             News,
             Page,
             PageCategory,
             PageComment,
             Post,
             Media,
             File,
             Link,
             Contact,
             User
]