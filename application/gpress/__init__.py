#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""
"""
HTTP_REFERER = http://localhost:8080/gp-admin/files
SERVER_SOFTWARE = Development/1.0
SCRIPT_NAME = 
REQUEST_METHOD = GET
PATH_INFO = /gp-admin/posts
SERVER_PROTOCOL = HTTP/1.0
QUERY_STRING = 
CONTENT_LENGTH = 
HTTP_ACCEPT_CHARSET = ISO-8859-1,utf-8;q=0.7,*;q=0.3
HTTP_USER_AGENT = Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0
HTTP_CONNECTION = keep-alive
HTTP_COOKIE = dev_appserver_login="test@example.com:True:185804764220139124118"
SERVER_NAME = localhost
REMOTE_ADDR = 127.0.0.1
PATH_TRANSLATED = C:\Documents and Settings\bornslippy\Documenti\Eclipse Workspace\GPress\src\application/controllers/admin/posts.py
SERVER_PORT = 8080
USER_IS_ADMIN = 1
CURRENT_VERSION_ID = 16.1
HTTP_HOST = localhost:8080
TZ = UTC
HTTP_CACHE_CONTROL = max-age=0
USER_EMAIL = test@example.com
HTTP_ACCEPT = application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5
USER_ID = 185804764220139124118
GATEWAY_INTERFACE = CGI/1.1
HTTP_ACCEPT_LANGUAGE = it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4
APPLICATION_ID = bb-29b
CONTENT_TYPE = application/x-www-form-urlencoded
AUTH_DOMAIN = gmail.com
"""
import os, datetime, tools

from urllib import urlencode

from application.models.settings import Settings
from application.utils.cookies import Cookies
import application.conf.django as django_app_settings

from google.appengine.api import users, memcache
from google.appengine.ext import db

os.environ['DJANGO_SETTINGS_MODULE'] = 'application.conf.django'
from django.conf import settings
settings._target = None



descriptions = {
		'dashboard': "From this page you can monitor the website.",
		'posts': "From this page you can manage and/or add your favourites posts.",
		'pages': "From this page you can manage and/or add your favourites pages.",
		'media': "From this page you can manage and/or add your favourites media files.",
		'files': "From this page you can manage and/or add your favourites files.",
		'links': "From this page you can manage and/or add your favourites links.",
		'comments': "From this page you can manage your content's comments.",
		'guestbook': "From this page you can manage and/or add your guestbook signs.",
		'themes': "From this page you can change the website theme.",
		'plugins': "From this page you can view your plugins",
		'users': "From this page you can manage and/or add new users.",
		'settings': "From this page you can change the website settings."
}

""" Detect debug mode """
Debug = os.environ["SERVER_SOFTWARE"].split("/")[0].lower() == "development"

""" Reserved permalinks """
RESERVED_PERMALINKS = [
  'logout','login','robots.txt',
  'sitemap.xml','sitemap.html',
  'sitemap.xml.gz','pages','posts',
  'blog','media','files','links']

""" User ranks """
BANNED = -2
INACTIVE = -1
ANONYMOUS = 0
GOOGLEUSER = 1
USER = 2
CONTRIBUTOR = 3
OPERATOR = 4
ADMINISTRATOR = 5
DEVELOPER = 6

class Registry(object):
	""" Main application handlers """
	site = None
	user = None
	page = None
	data = ""
	cookies = {}
	
	""" Request data """
	module = None
	controller = None
	action = None
	id = None
	admin = None
	
	got_feed = True
	
	error = {'message':None}
	
	def __init__(self, handler=None, authrank=ANONYMOUS, privacy=0):
		os.environ['DJANGO_SETTINGS_MODULE'] = 'application.conf.django'
		if handler:
			self.request = handler.request
		
		self.contents = ContentsFactory()
		self.public_contents = ContentsFactory(0)
		self.listing_contents = ContentsFactory(1)
		self.user_instance = users.get_current_user()
			
		from application.models.users import User
		""" Get site settings """
		self.site = Site()
		
		if users.get_current_user():
			""" Get the current google user """
			self.user = User.get_by_key_name(users.get_current_user().user_id())
			
		if not self.user and users.get_current_user():
			if User.all().filter('email =', users.get_current_user().email()).count(1):
				self.user = User.all().filter('email =', users.get_current_user().email()).fetch(1)[0]
		
		if self.site.register_anonymous and not self.user and users.get_current_user():
			self.user = User.get_or_insert(users.get_current_user().user_id(),
																					name=users.get_current_user().nickname(),
																					email=users.get_current_user().email(),
																					rank=DEVELOPER if users.is_current_user_admin() else GOOGLEUSER)
				
		elif not self.user:
			""" Get anonymous user """
			self.user = User.get_or_insert("nobody@example.com",
																						name="Anonymous",
																						email="nobody@example.com",
																						rank=ANONYMOUS)
	
		""" Authenticate user """
		if users.is_current_user_admin():
			self.user.rank = 6
		elif authrank and handler:
			if not self.user.auth(authrank):
				handler.redirect('/gp-admin/errors/forbidden?%s' %
									 urlencode({'ref': handler.request.uri}), True)
			
		""" Analyze the request """
		request = tools.get_requested_object_list()
		if len(request) and request[0] == 'gp-admin':
			self.admin = True
			if len(request) >= 2 and request[1] == 'plugins':
					self.module = self.controller = self.id = request[2]
					if len(request) >= 4:
							self.action = request[3]
			elif len(request) >= 2:
					self.controller = request[1]
					self.id = request[1]
					if len(request) >= 3:
							self.action = request[2]
			else:
				self.controller = 'dashboard'
				self.id = 'dashboard'
		else:
			if len(request) >= 1 and request[0] == 'plugins':
					self.module = self.controller = self.id = request[1]
					if len(request) >= 3:
							self.action = request[2]
			elif len(request) >= 1:
					self.controller = request[0]
					self.id = request[0]
					if len(request) >= 2:
							self.action = request[1]
			else:
				self.controller = 'home'
				self.id = 'home'
		

		self.plugins = []
		if self.module: self.got_feed = False
		
		""" Get the requested page """
		self.page = Page()
		
		if self.controller in ['pages','posts','files','media','links']:
			self.got_feed = True
		
		if self.id in descriptions:
				self.page.description = descriptions.get(self.id)
		
		if not self.page.title and self.id:
			self.page.title = self.id.capitalize()
		
	def set(self, name, value=None):
		self.__setattr__(name, value)
		
	def get(self, name):
		self.__getattr__(name)
		
	def has_key(self, name):
		try: return self.get(name)
		except: return False
						
	def google_analytics_code(self):
		if self.site.google_analytics_id:
			return """<!-- Google Analytics --><script type="text/javascript" src="http://www.google-analytics.com/ga.js"></script><script type="text/javascript">try {var pageTracker = _gat._getTracker("%s");pageTracker._trackPageview();} catch(err) {}</script><!-- Google Analytics Ends -->""" % self.site.google_analytics_id
		return ""


class Site(object):
		debug = False
		title = "My new WebSite!"
		subtitle = "we love GPress"
		description = "This is my new GPress WebSite! I spent just 3 minutes to install it, wow :) GPress is a WordPress clone and it's written in python in order to be GAE(Google App Engine) full compatible."
		keywords = "gpress,wordpress clone,clone,blog,blog software,google app engine,gae, cms, web site, website, web cms, website cms, web site cms"
		robots = "index,follow"
		googlebot = "index,follow"
		revisit_after = "30 Days"
		last_update = "2009-08-04 20:55:10"
		author = "Unknown"
		author_mail = "author@example.com"
		publisher = "Unknown GPress User"
		reply_to = "blog@example.com"
		admin_mail = "admin@example.com"
		generator = "GPress 1.9." + os.environ["CURRENT_VERSION_ID"]
		
		news_limit = 1000
		posts_limit = 10
		pages_limit = 1000
		files_limit = 1000
		media_limit = 1000
		links_limit = 1000
		contacts_limit = 1000
		
		feeds_posts_limit = 20
		feed_details = 0
		read_encoding = "UTF-8"
		date_format = "F j, Y"
		time_format = "g:i a"
		country = "Italy"
		language = "en"
		timezone = "Etc/GMT-2"
		homepage = "0"
		sitemap_shared = "1"
		sitemap_priority = "0.3"
		sitemap_freq = "monthly"
		feeds_shared = "1"
		protection = "2"
		register_open = False
		is_ajax_request = False
		register_anonymous = True
		
		default_new_post_feeds_shared = "1"
		default_new_page_feeds_shared = "1"
		default_new_file_feeds_shared = "1"
		default_new_link_feeds_shared = "1"
		default_new_media_feeds_shared = "1"
		default_new_news_feeds_shared = "1"
		
		default_new_post_sitemap_shared = "1"
		default_new_page_sitemap_shared = "1"
		default_new_file_sitemap_shared = "1"
		default_new_link_sitemap_shared = "1"
		default_new_media_sitemap_shared = "1"
		default_new_news_sitemap_shared = "1"
		
		default_new_post_sitemap_priority = "0.7"
		default_new_page_sitemap_priority = "0.8"
		default_new_file_sitemap_priority = "0.3"
		default_new_link_sitemap_priority = "0.1"
		default_new_media_sitemap_priority = "0.7"
		default_new_news_sitemap_priority = "0.8"
		
		default_new_post_sitemap_freq = "daily"
		default_new_page_sitemap_freq = "monthly"
		default_new_file_sitemap_freq = "monthly"
		default_new_link_sitemap_freq = "monthly"
		default_new_media_sitemap_freq = "monthly"
		default_new_news_sitemap_freq = "monthly"
		
		default_new_post_status = "0"
		default_new_page_status = "0"
		default_new_file_status = "0"
		default_new_link_status = "0"
		default_new_media_status = "0"
		default_new_news_status = "0"
		
		news_enabled = "0"
		posts_enabled = "0"
		pages_enabled = "0"
		files_enabled = "0"
		media_enabled = "0"
		links_enabled = "0"
		contacts_enabled = "0"
		
		google_analytics_id = ""
		"""
		url = ""
		homepage_post = ""
		homepage_page = ""
		theme = ""
		theme_name = ""
		theme_path = ""
		templates_path = ""
		admin_templates_path = ""
		themes_path = ""
		root_path = ""
		"""
		
		def set(self, name, value):
				self.__setattr__(name, value)
		
		def template_vars(self):
			ret =	[]
			for name in self.__dict__.keys():
				ret.append({'name': name, 'value': self.__dict__.get(name)})
			ret = [{'name': '&nbsp;', 'value': self.generator + " (2009/12/22)"}, # data ultima modifica cartella root
							 {'name': 'Title', 'value': self.title},
							 {'name': 'Description', 'value': self.description},
							 {'name': 'KeyWords', 'value': self.keywords},
							 {'name': 'Publisher', 'value': self.publisher},
							 {'name': 'Theme', 'value': self.theme_name},
							 {'name': 'G-Analytics', 'value': "Enabled" if self.google_analytics_id else "Disabled"}]
			if self.homepage == "-1":
				ret.append({'name': 'Homepage', 'value': 'Intro'})
			elif self.homepage == "0":
				ret.append({'name': 'Homepage', 'value': 'Latest %s posts' % self.posts_limit})
			elif self.homepage == "1":
				from application.models.posts import Post
				ret.append({'name': 'Homepage', 'value': 'Post: %s' % Post.get(db.Key(self.homepage_post)).title})
			elif self.homepage == "2":
				from application.models.pages import Page
				ret.append({'name': 'Homepage', 'value': 'Page: %s' % Page.get(db.Key(self.homepage_page)).title})
			return ret
		
		def __init__(self):
				self.date = datetime.datetime.now()
				
				self.url = "http://" + os.environ["HTTP_HOST"]
				self.debug = Debug

				self.themes = []
				self.themes.append(Theme(name='GPress Theme',
																 id='gpress',
																 path='/',
																 url='',
																 intro=True))
				self.themes.append(Theme(name='29b - Bed and Breakfast',
																 id='29b',
																 path='/',
																 url='',
																 intro=True))
				
				self.basepath = os.path.abspath("%s%s..%s..%s.." % (os.path.dirname(os.environ["PATH_TRANSLATED"]),os.sep,os.sep,os.sep))
				self.themes_path = "%s/themes/" % self.basepath 
				options = memcache.get("gp_settings")
				if options is None:
					options = Settings.all().fetch(1000)
					memcache.add("gp_settings", options, 86400)

				for option in options:
						self.set(option.field, option.value)

				django_app_settings.LANGUAGE_CODE = self.language
				
				if 'theme' in self.__dict__:
						self.theme_url = "themes/%s" % self.theme
						self.theme_path = '%s%s/' % (self.themes_path, self.theme)
						self.templates_path = '%sviews/public/' % self.theme_path

						theme_admin = "%s/views/admin/" % (self.theme_path)
						if os.path.lexists(theme_admin):
								self.admin_templates_path = theme_admin
						else:
								self.admin_templates_path = '%s/application/views/admin/' % self.basepath
								
						theme_admin_css = "%s/static/gp-admin/" % (self.theme_path)
						if os.path.lexists(theme_admin_css):
								self.admin_theme_url = "/themes/%s/static" % self.theme

						self.theme_instance = self.themes[1]

				else:
						self.theme_instance = self.themes[0]
						self.theme = ''
						self.theme_name = "GPress theme"
						self.theme_url = "/"
						self.theme_path = '%s/../application/'% self.themes_path
						self.templates_path = '%s/views/public/' % self.theme_path
						self.admin_templates_path = '%s/views/admin/' % self.theme_path


class Page(object):
	title = None
	description = None
	body = None
	def process_page(self, page):
		try:
			for k in page.properties().keys():
				if str(k) in ['body','description','title'] and str(page.__getattribute__(k)).strip() != '':
					self.__setattr__(k, page.__getattribute__(k))
		except:
			try:
				for k in dict(page).keys():
					if str(k) in ['body','description','title'] and str(page.__getattribute__(k)).strip() != '':
						self.__setattr__(k, page.__getattribute__(k))
			except:
				return page


class ContentsFactory(object):
	def __init__(self, status=-1):
		self.public_status = int(status)
	
	def news(self):
		from application.models.news import News
		return News.all().filter('publish_status >=', self.public_status).order('-publish_status').order('-publish_date').fetch(1000)
		
	def posts(self):
		from application.models.posts import Post
		return Post.all().filter('publish_status >=', self.public_status).order('-publish_status').order('-publish_date').fetch(1000)
		
	def posts_categories(self):
		from application.models.posts import PostCategory
		return PostCategory.all().fetch(1000)
		
	def pages(self):
		from application.models.pages import Page
		return Page.all().filter('publish_status >=', self.public_status).order('-publish_status').order('-publish_date').fetch(1000)
		
	def pages_categories(self):
		from application.models.pages import PageCategory
		return PageCategory.all().fetch(1000)
		
	def files(self, publish_status=0):
		from application.models.files import File
		return File.all().filter('publish_status >=', self.public_status).fetch(1000)
		
	def files_categories(self):
		from application.models.files import FileCategory
		return FileCategory.all().fetch(1000)
		
	def media(self):
		from application.models.media import Media
		return Media.all().filter('publish_status >=', self.public_status).fetch(1000)
		
	def media_albums(self):
		from application.models.media import MediaAlbum
		return MediaAlbum.all().fetch(1000)
		
	def links(self):
		from application.models.links import Link
		return Link.all().filter('publish_status >=', self.public_status).fetch(1000)
		
	def links_categories(self):
		from application.models.links import LinkCategory
		return LinkCategory.all().fetch(1000)
		
	def contacts(self):
		from application.models.contacts import Contact
		return Contact.all().fetch(1000)
		
	def users(self, guest=1,users_messages=None,site_messages=None):
		from application.models.users import User
		u = User.all()
		if users_messages is not None:
			u.filter('users_messages =', bool(int(users_messages)))
		if site_messages is not None:
			u.filter('site_messages =', bool(int(site_messages)))
		if not guest:
			u.filter('rank >',  1)
		return u.fetch(1000)
	
	def users_list(self):
		ret = []
		from application.models.users import User
		us = User.all().fetch(1000)
		for user in us:
			ret.append(user.email)
		return ret
		




























class Module(object):
		id = ''
		name = ''
		path = ''
		image = False
		logo_image = False
		url = ''
		admin_url = ''
		def __init__(self, *pargs, **kargs):
				for arg in kargs:
						self.__setattr__(arg, kargs.__getitem__(arg))

							
modules = []
modules.append(Module(name='Headers',
											id='pages-images',
											path='/',
											image=True,
											logo_image=True))


class Theme(object):
		id = ''
		name = ''
		path = ''
		url = ''
		def __init__(self, *pargs, **kargs):
				for arg in kargs:
						self.__setattr__(arg, kargs.__getitem__(arg))


themes = []
themes.append(Module(name='GPress Theme',
										 id='gpress',
										 path='/',
										 url='',
										 image=None,
										 logo_image=None))
