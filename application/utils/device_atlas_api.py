#!/usr/bin/env python
'''
$Id: api.py 3810 2008-07-30 10:57:38Z rcremin $

Used to load the recognition tree and perform lookups of all properties, or
individual properties. Typical usage is as follows: 

    >>> import api
    >>> dir(api)
    ['DaApi', '__author__', '__builtins__', '__contributors__', '__doc__', '__file__', '__license__', '__name__', '__version__', 'simplejson', 'string', 'sys', 'time']
    >>> da = api.DaApi()
    >>> tree = da.getTreeFromFile('DeviceAtlas.json')
    >>> da.getTreeRevision(tree)
    2347
    >>> ua = 'SonyEricssonW850i/R1GB Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1'
    >>> da.getProperties(tree, ua)
    {u'gprs': '1', u'mpeg4': '1', u'drmOmaForwardLock': '1', u'umts': '0', u'displayWidth': '240', u'mp3': '1', u'markup.xhtmlMp11': '1', u'markup.xhtmlMp10': '1', u'markup.xhtmlMp12': '0', u'id': '205009', u'memoryLimitMarkup': '45000', u'midiPolyphonic': '1', u'image.Gif87': '1', u'csd': '1', u'3gpp': '1', u'qcelp': '0', u'wmv': '1', u'markup.xhtmlBasic10': '1', u'https': '1', u'image.Gif89a': '0', u'3gpp2': '0', u'hscsd': '0', u'midiMonophonic': '1', u'drmOmaSeparateDelivery': '1', u'displayColorDepth': '18', u'vendor': 'Sony Ericsson', u'image.Jpg': '1', u'uriSchemeTel': '1', u'mobileDevice': '1', '_unmatched': 'R1GB Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1', u'hsdpa': '0', u'amr': '1', u'model': 'W850i', u'drmOmaCombinedDelivery': '1', u'aac': '0', u'mpeg4InVideo': '1', u'image.Png': '1', u'edge': '0', u'h263Type0InVideo': '1', u'displayHeight': '320', u'aacInVideo': '1', '_matched': 'SonyEricssonW850i/'}
    >>> 
    >>> da.getProperty(tree, ua, 'displayWidth')
    240
    >>> da.getProperty(tree, ua, 'displayHeight')
    320
    >>> da.getPropertiesAsTyped(tree, ua)
    {u'gprs': True, u'mpeg4': True, u'drmOmaForwardLock': True, u'umts': False, u'displayWidth': 240, u'mp3': True, u'markup.xhtmlMp11': True, u'markup.xhtmlMp10': True, u'markup.xhtmlMp12': False, u'id': 205009, u'memoryLimitMarkup': 45000, u'midiPolyphonic': True, u'image.Gif87': True, u'csd': True, u'3gpp': True, u'qcelp': False, u'wmv': True, u'markup.xhtmlBasic10': True, u'https': True, u'image.Gif89a': False, u'3gpp2': False, u'hscsd': False, u'midiMonophonic': True, u'drmOmaSeparateDelivery': True, u'displayColorDepth': 18, u'vendor': 'Sony Ericsson', u'image.Jpg': True, u'uriSchemeTel': True, u'mobileDevice': True, '_unmatched': 'R1GB Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1', u'hsdpa': False, u'amr': True, u'model': 'W850i', u'drmOmaCombinedDelivery': True, u'aac': False, u'mpeg4InVideo': True, u'image.Png': True, u'edge': False, u'h263Type0InVideo': True, u'displayHeight': 320, u'aacInVideo': True, '_matched': 'SonyEricssonW850i/'}
>>> da.getApiRevision()
    2426
    >>> da.listProperties(tree)
    {u'gprs': 'boolean', u'memoryLimitDownload': 'integer', u'mpeg4': 'boolean', u'drmOmaForwardLock': 'boolean', u'memoryLimitEmbeddedMedia': 'integer', u'umts': 'boolean', u'displayWidth': 'integer', u'mp3': 'boolean', u'markup.xhtmlMp11': 'boolean', u'markup.xhtmlMp10': 'boolean', u'cookieSupport': 'boolean', u'markup.xhtmlMp12': 'boolean', u'id': 'integer', u'image.Gif89a': 'boolean', u'midiPolyphonic': 'boolean', u'image.Gif87': 'boolean', u'isChecker': 'boolean', u'csd': 'boolean', u'3gpp': 'boolean', u'qcelp': 'boolean', u'wmv': 'boolean', u'drmOmaSeparateDelivery': 'boolean', u'isSpam': 'boolean', u'markup.xhtmlBasic10': 'boolean', u'aacLtpInVideo': 'boolean', u'version': 'string', u'amrInVideo': 'boolean', u'qcelpInVideo': 'boolean', u'https': 'boolean', u'stylesheetSupport': 'string', u'memoryLimitMarkup': 'integer', u'3gpp2': 'boolean', u'hscsd': 'boolean', u'midiMonophonic': 'boolean', u'image.Png': 'boolean', u'displayColorDepth': 'integer', u'vendor': 'string', u'image.Jpg': 'boolean', u'uriSchemeTel': 'boolean', u'isRobot': 'boolean', u'mobileDevice': 'boolean', u'isBrowser': 'boolean', u'isFilter': 'boolean', u'isDownloader': 'boolean', u'hsdpa': 'boolean', u'amr': 'boolean', u'displayHeight': 'integer', u'h263Type3InVideo': 'boolean', u'drmOmaCombinedDelivery': 'boolean', u'aac': 'boolean', u'scriptSupport': 'string', u'mpeg4InVideo': 'boolean', u'awbInVideo': 'boolean', u'edge': 'boolean', u'h263Type0InVideo': 'boolean', u'model': 'string', u'aacInVideo': 'boolean'}
    >>> da.getPropertiesAsTyped(tree, ua)['mobileDevice']
    True
    >>> da.getPropertiesAsTyped(tree, ua)['vendor']
    'Sony Ericsson'
    >>> da.getPropertiesAsTyped(tree, ua)['model']
    'W850i'
    >>> len(da.getPropertiesAsTyped(tree, ua))
    4

In some contexts, the user-agent you want to recognise may have been provided 
in a different header. Opera's mobile browser, for example, makes requests via 
an HTTP proxy, which rewrites the headers. in that case, the original device's
user-agent is in the HTTP_X_OPERAMINI_PHONE_UA header.

'''

__version__ = '1.3.1 Build $Rev: 3810 $' 
__url__ = 'http://deviceatlas.com'
__author__ = 'Ronan Cremin <http://dev.mobi/>'
__contributors__ = ['Adrian Hope-Balie <http://dev.mobi/>','James Pearce <http://dev.mobi/>']
__license__ = '''Copyright (c) 2008, mTLD (dotMobi), All rights reserved.
Portions copyright (c) 2008 by Argo Interactive Limited.
Portions copyright (c) 2008 by Nokia Inc.
Portions copyright (c) 2008 by Telecom Italia Mobile S.p.A.
Portions copyright (c) 2008 by Volantis Systems Limited.
Portions copyright (c) 2002-2008 by Andreas Staeding.
Portions copyright (c) 2008 by Zandan.
'''
                    

import simplejson, time, string, sys
from daExceptions import InvalidPropertyException, IncorrectPropertyTypeException, JsonException, UnkownPropertyException  


class DaApi:

    def listToDictionary(self, tree):
        '''Recursively changes lists in passed tree to dictionaries
        '''
        if type(tree) == type( {} ):
            for key, val in tree.iteritems():
                if (type(tree[key]) == type({})) or (type(tree[key]) == type([])):
                    tree[key] = self.listToDictionary(tree[key])
        elif type(tree) == type( [] ):
            treeCopy = tree[:]
            tree = {}

            for i in range(0, len(treeCopy)):
                tree[str(i)] = treeCopy[i]
            tree = self.listToDictionary(tree)

        return tree


    def getTreeFromString(self, json):
        '''	Returns a tree from a JSON string return array tree
        json is a string of json data.
        Throws RunTimeError
        '''
        version = string.split(string.split(sys.version)[0], ".")
        if map(int, version) < [2, 3, 0]:
            raise ImportError('Version 2.3 or later of Python is required')

        tree = simplejson.loads(json)

        if tree == {}:
            raise JsonException('Failed to load JSON')
        elif ( not tree.has_key('$') ):
            raise JsonException('Bad data in JSON')
        elif float(tree['$']['Ver']) < 0.7:
            raise JsonException('JSON file must be version 0.7 or later. Please download a more recent version')

        pr = {}
        pn = {}
        for value in tree['p']:
            pr[value] = tree['p'].index(value)
            pn[value[1:]] = tree['p'].index(value)
        tree['pr'] = pr
        tree['pn'] = pn

        return self.listToDictionary(tree)


    def getTreeFromFile(self, file):
        '''Returns a tree from a JSON file
        file is the location of the file to read
        '''
        json = open(file, 'r').read()
        if json == '':
            raise JsonException('Unable to load file: %s' % file)

        return self.getTreeFromString(json)


    def getTreeRevision(self, tree):
        '''Returns revision number of the tree
        tree is a previously generated tree
        '''
        return self._getRevisionFromKeyword(tree['$']['Rev'])


    def getApiRevision(self):
        '''Returns revision number of the API
        tree is a previously generated tree
        '''
        return self._getRevisionFromKeyword('$Rev: 3810 $')


    def listProperties(self, tree):
        '''Returns array of known property names. Returns all properties
        available for all user agents in this tree, with their data type
        names
        tree is previously generated tree
        '''
        types = {
            's': 'string',
            'b': 'boolean',
            'i': 'integer',
            'd': 'date',
            'u': 'unknown' }
        listPropertiesArray = {}
        for key, property in tree['p'].iteritems():
            listPropertiesArray[property[1:]] = types[property[0]]

        return listPropertiesArray

   
    def getProperties(self, tree, userAgent):
        '''Returns an array of known properties (as strings) for the UA

        tree is previously generated tree
        userAgent is string from devices User-Agent header
        '''
        return self._getProperties(tree, userAgent, False)


    def getPropertiesAsTyped(self, tree, userAgent):
        '''Returns an array of known properties (as typed) for the UA

        tree is previously generated tree
        userAgent is string from devices User-Agent header
        '''
        return self._getProperties(tree, userAgent, True)


    def getProperty(self, tree, userAgent, property):
        '''Returns a value for the named property of this user agent

        tree is previously generated tree
        userAgent is string from devices User-Agent header
        property is the name of the property to return
        '''
        return self._getProperty(tree, userAgent, property, False)


    def getPropertyAsBoolean(self, tree, userAgent, property):
        '''Strongly-typed property accessor. Returns boolean property. 
        Throws exception of property is actually a different type

        tree is previously generated tree
        userAgent is string from devices User-Agent header
        property is the name of the property to return
        '''
        self._propertyTypeCheck(tree, property, 'b', 'boolean')
        return self._getProperty(tree, userAgent, property, True)


    def getPropertyAsDate(self, tree, userAgent, property):
        '''Strongly-typed property accessor. Returns date property. 
        Throws exception of property is actually a different type

        tree is previously generated tree
        userAgent is string from devices User-Agent header
        property is the name of the property to return
        '''
        self._propertyTypeCheck(tree, property, 'd', 'string')
        return self._getProperty(tree, userAgent, property, True)


    def getPropertyAsInteger(self, tree, userAgent, property):
        '''Strongly-typed property accessor. Returns integer property. 
        Throws exception of property is actually a different type

        tree is previously generated tree
        userAgent is string from devices User-Agent header
        property is the name of the property to return
        '''

        self._propertyTypeCheck(tree, property, 'i', 'integer')
        return self._getProperty(tree, userAgent, property, True)


    def getPropertyAsString(self, tree, userAgent, property):
        '''Strongly-typed property accessor. Returns string property. 
        Throws exception of property is actually a different type

        tree is previously generated tree
        userAgent is string from devices User-Agent header
        property is the name of the property to return
        '''
        self._propertyTypeCheck(tree, property, 's', 'string')
        return self._getProperty(tree, userAgent, property, True)


    # PRIVATE METHODS
    #
    #

    def _getRevisionFromKeyword(self, keyword):
        '''Returns cleaned up version of SVN revision string as integer
        keyword is string
        '''
        return int(keyword[6:].replace('$', '').strip())


    def _getProperties(self, tree, userAgent, typedValues):
        '''Returns an array of known properties for the user agent
        Allows the values of properties to be forced to be strings.

        arrray tree is a previously generated tree
        string userAgent is device's User-Agent header
        boolean typedValues indicates whether values in the hashmap are typed
        '''
        idProperties = {}
        matched = ''
        sought = None
        sought, matched = self._seekProperties(tree['t'], userAgent.strip(), idProperties, sought, matched)
        properties = {}

        for id, value in idProperties.iteritems():
            if typedValues:
                properties[self._propertyFromId(tree, id)] = self._valueAsTypedFromId(tree, value, id)
            else:
                properties[self._propertyFromId(tree, id)] = str(self._valueFromId(tree, value))

        properties['_matched'] = matched
        properties['_unmatched'] = userAgent[len(matched):]
        return properties


    def _getProperty(self, tree, userAgent, property, typedValue):
        '''Returns a value for the named property for this user agent
        Allows the value to be typed or forced as a string

        arrray tree is a previously generated tree
        string userAgent is device's User-Agent header
        boolean typedValues indicates whether values in the hashmap are typed
        
        Raises InvalidPropertyException if property not found
        '''
        propertyId = self._idFromProperty(tree, property)
        idProperties = {}
        sought = { propertyId: True }
        matched = ''
        unmatched = ''
        sought, matched = self._seekProperties(tree['t'], userAgent.strip(), idProperties, sought, matched)
        if len(idProperties) == 0:
            raise InvalidPropertyException('The property %s is invalid for the User Agent %s' % (property, userAgent))
        return self._valueFromId(tree, idProperties[propertyId]) 


    def _idFromProperty(self, tree, property):
        '''Return the coded ID for a property's name

        tree is a previously generated tree
        property is a string

        Raises UnkownPropertyException if property not found
        '''
        if (tree['pn'].has_key(property)):
            return str(tree['pn'][property])
        else:
            raise UnkownPropertyException('The property %s is not known in this tree' % property)
            

    def _propertyFromId(self, tree, id):
        '''Return the name for a property's coded id

        tree is previously generated tree
        '''
        return  tree['p'][id][1:]


    def _propertyTypeCheck(self, tree, property, prefix, typeName):
        '''Checks that the property is of the supplied type or 
        throws exception

        array tree Previously generated HashMap tree
        array property The name of the property to return
        string prefix The type prefix (i for integer)
        string typeName Easy readable type name
        '''
        if not tree['pr'].has_key(prefix + property):
            print 'prefix, property', prefix, property
            print tree['pr']
            raise IncorrectPropertyTypeException('The property %s is not of type %s' % (property, typeName))


    def _seekProperties(self, node, string, properties, sought, matched):
        '''Seek properties for a UA within a node
        This is designed to be recursed, and only externally called with the
        node representing the top of the tree

        node is array
        string is string
        properties is properties found
        sought is properties being sought
        matched is part of UA that has been matched
        '''
        unmatched = string
        if node.has_key('d'):
            if (sought != None and len(sought) == 0):
                return sought, matched
            for property, value  in node['d'].iteritems():
                if sought == None or sought.has_key(property):
                    properties[property] = value
                if (sought != None and 
                    ( (not node.has_key('m')) or (node.has_key('m') and (not node['m'].has_key(property)) ) ) ):
                    if sought.has_key(property):
                        del sought[property]

        if node.has_key('c'):
            for c in range(1, len(string)+1):
                seek = string[0:c]
                if node['c'].has_key(seek):
                    matched += seek
                    sought, matched = self._seekProperties(node['c'][seek], string[c:], properties, sought, matched)
                    break
            
        return sought, matched


    def _valueAsTypedFromId(self, tree, id, propertyId):
        ''' Returns the property value typed

        array tree
        string id
        string propertyId
        '''
        obj = tree['v'][str(id)]
        if tree['p'][propertyId][0] == 's':
            obj = str(obj)
        elif tree['p'][propertyId][0] == 'b':
            obj = bool(obj)
        elif tree['p'][propertyId][0] == 'i':
            obj = int(obj)
        elif tree['p'][propertyId][0] == 'd':
            obj = str(obj)
        return obj


    def _valueFromId(self, tree, id):
        '''Returns the value for a value's coded ID

        array tree
        string id
        '''
        return tree['v'][str(id)]


if __name__ == '__main__':
    print __doc__
    print 'Version:', __version__
    print 'License:', __license__



