#!/usr/bin/python2.5
# -*- coding: utf-8 -*-
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds DN
@project: Image-Tools
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

import httplib, urllib, sys

from application.utils.jscompress.jsmin import jsmin

class JS(object):
  params = {}
  headers = {"Content-type": "application/x-www-form-urlencoded" }
  
  def __init__(self, options):
    params = {
      'compilation_level': 'ADVANCED_OPTIMIZATIONS',
      'output_format': 'text',
      'output_info': 'compiled_code',
      'formatting': 'pretty_print',
      'closure_compiler': False
    }
    for k in params:
      self.params.setdefault(k, options[k])
    
    for k in options:
      self.params.setdefault(k, options[k])
  
  def compress(self):
    post_vars = []
    for k in self.params:
      post_vars.append((k, self.params.get(k)))
      
    if self.params.has_key('js_code') or self.params.has_key('code_url'):
      if self.params["closure_compiler"]:
        try:
          conn = httplib.HTTPConnection('closure-compiler.appspot.com')
          conn.request('POST', '/compile', urllib.urlencode(post_vars), self.headers)
          
          data = conn.getresponse().read()
          conn.close
        except:
          data = jsmin(self.params["js_code"] if self.params.has_key("js_code") else self.params["code_url"])
      else:
        data = jsmin(self.params["js_code"] if self.params.has_key("js_code") else self.params["code_url"])

    return data