#!/usr/bin/python
# -*- coding: utf-8 -*-
#
import os, sys
import hashlib

def md5sum(fileName, excludeLine="", includeLine=""):
    """Compute md5 hash of the specified file"""
    m = hashlib.md5()
    try:
        fd = open(fileName,"rb")
    except IOError:
        return False
    content = fd.readlines()
    fd.close()
    for eachLine in content:
        if excludeLine and eachLine.startswith(excludeLine):
            continue
        m.update(eachLine)
    m.update(includeLine)
    return m.hexdigest()

if __name__ == "__main__":
    for eachFile in sys.argv[1:]:
        print "%s %s" %(md5sum(eachFile), eachFile)