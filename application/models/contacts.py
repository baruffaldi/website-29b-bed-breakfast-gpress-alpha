#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

from datetime import datetime
import os

from urllib import urlencode

from application.gpress import tools

from application.gpress import models
from google.appengine.ext import db

class Contact(models.SerializableModel):
	""" Timelines """
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update_date = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	
	""" Content Info """
	name = db.StringProperty()
	address = db.StringProperty()
	location = db.StringProperty()
	cap = db.StringProperty()
	state = db.StringProperty()
	
	""" Utils """
	def code(self, width=580, height=190):
		if self.address and self.location:
			return '<iframe width="%i" height="%i" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=%s,+%s+%s,+%s&amp;ie=UTF8&amp;t=p&amp;hq=&amp;hnear=%s,+%s+%s,+%s&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=%s,+%s+%s,+%s&amp;ie=UTF8&amp;t=p&amp;hq=&amp;hnear=%s,+%s+%s,+%s&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Visualizzazione ingrandita della mappa</a></small>' % (width, height, self.address, self.cap, self.location, self.state, self.address, self.cap, self.location, self.state, self.address, self.cap, self.location, self.state, self.address, self.cap, self.location, self.state)
		return ''
	
	def get_user(self):
		from application.models.users import User
		return User.get_by_key_name(self.user)
	
	""" Utils """
	def list(self):
		return list(self)
	
	def get_update_user(self):
		from application.models.users import User
		return User.get_by_key_name(self.update_user)
		
	def get_kind(self):
		return 'Contact'
		
	def update(self, values_dict={}):
		for name in values_dict:
			value = values_dict[name]
			if name == "date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "update_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.update_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name != 'key' and name in self.properties().keys():
				self.__setattr__(name, value.strip())