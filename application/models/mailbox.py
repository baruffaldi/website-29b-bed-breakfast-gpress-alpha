#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

no_html_fields = [
                  'name',
                  'reference',
                  'body'
                  ]

skip_fields = ['key' ]

from application.gpress import models
from google.appengine.ext import db


class Message(models.SerializableModel):
  """ Timelines """
  user = db.UserProperty(auto_current_user_add=True)
  date = db.DateTimeProperty(auto_now_add=True)

  """ Source Info """
  name = db.StringProperty()
  reference = db.StringProperty()
  
  """ Destination Info """
  destinations = db.StringListProperty()
  
  """ Message """
  body = db.TextProperty(required=True)
  
  noticed = db.BooleanProperty(default=False)
  read = db.BooleanProperty(default=False)
    
  """ Utils """
  def get_kind(self):
    return 'Message'
    
  def update(self, values_dict={}):
    for name in values_dict:
      value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
      if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
        value = value[0]
      if name not in skip_fields and name in self.properties().keys():
        if name == 'destinations':
          self.destinations = list(value)
        elif isinstance(self.__getattribute__(name), db.IntegerProperty):
          self.__setattr__(name, int(str(value).strip()))
        elif isinstance(self.__getattribute__(name), db.FloatProperty):
          self.__setattr__(name, float(str(value).strip()))
        else:
          self.__setattr__(name, str(value).strip())