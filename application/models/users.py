#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  GPress - GAE WebSite CMS
#  Copyright (C) 2009 Filippo Baruffaldi
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

no_html_fields = [
                  'name'
                  ]

skip_fields = [
               'key'
               ]

from application.gpress import models

from google.appengine.ext import db
from google.appengine.api import mail, users

class User(models.SerializableModel):
  user = db.UserProperty(auto_current_user_add=True)
  date = db.DateTimeProperty(auto_now_add=True)
  update_date = db.DateTimeProperty(auto_now=True)
  update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
  
  name = db.StringProperty()
  email = db.EmailProperty(required=True)
  rank = db.IntegerProperty(default=1)
  language = db.StringProperty()
  
  users_messages = db.BooleanProperty(default=False)
  site_messages = db.BooleanProperty(default=False)
  site_emails = db.BooleanProperty(default=False)
  
  """ Utils """
  def get_kind(self):
    return 'User'
  
  def nickname(self):
    return self.name
    
  def update(self, values_dict={}, user_rank=1):
    self.users_messages = False
    self.site_emails = False
    self.site_messages = False
    for name in values_dict:
      value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
      if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
        value = value[0]
      if name == "mail" and mail.is_email_valid(value):
        self.email = value
      elif name == "users_messages":
        self.users_messages = bool(int(value))
      elif name == "site_messages":
        self.site_messages = bool(int(value))
      elif name == "site_emails":
        self.site_emails = bool(int(value))
      elif name == "rank":
        if int(value) <= user_rank:
          self.rank = int(value)
      elif name not in skip_fields and name in self.properties().keys():
        if isinstance(self.__getattribute__(name), db.IntegerProperty):
          self.__setattr__(name, int(str(value).strip()))
        elif isinstance(self.__getattribute__(name), db.FloatProperty):
          self.__setattr__(name, float(str(value).strip()))
        else:
          self.__setattr__(name, str(value).strip())
  
  """ Authentication Helpers """
  def found(self):
    return self.has_key()
  
  def auth(self, level):
    return self.rank >= level
  
  def is_googleuser(self):
    return self.rank >= 1
  
  def is_user(self):
    return self.rank >= 2
  
  def is_contributor(self):
    return self.rank >= 3
  
  def is_operator(self):
    return self.rank >= 4
  
  def is_administrator(self):
    return self.rank >= 5
  
  def is_developer(self):
    return self.rank >= 6
  
  """ Contents Counters """
  def posts_count(self):
    from application.models import posts
    return posts.Post.all(keys_only=True).filter('user =', users.User(email=self.email)).count(1000)
  
  def pages_count(self):
    from application.models import pages
    return pages.Page.all(keys_only=True).filter('user =', users.User(email=self.email)).count(1000)
  
  def media_count(self):
    from application.models import media
    return media.Media.all(keys_only=True).filter('user =', users.User(email=self.email)).count(1000)
  
  def files_count(self):
    from application.models import files
    return files.File.all(keys_only=True).filter('user =', users.User(email=self.email)).count(1000)
  
  def links_count(self):
    from application.models import links
    return links.Link.all(keys_only=True).filter('user =', users.User(email=self.email)).count(1000)
  
  def contacts_count(self):
    from application.models import contacts
    return contacts.Contact.all(keys_only=True).filter('user =', users.User(email=self.email)).count(1000)
  
  def messages_count(self):
    from application.models import mailbox
    if self.site_messages:
      new_m = mailbox.Message.all().fetch(1000)
      new_m_c = 0
      for m in new_m:
        if str(self.key()) in m.destinations or self.site_messages and not m.destinations:
          new_m_c += 1
    else:
      new_m_c = mailbox.Message.all().filter('destinations IN', [self.key()]).count(1000)
    
    return new_m_c
  
  def new_messages_count(self):
    from application.models import mailbox
    new_m = mailbox.Message.all().filter('read =', False).fetch(1000)
    new_m_c = 0
    for m in new_m:
      if str(self.key()) in m.destinations or self.site_messages and not m.destinations:
        new_m_c += 1
    
    return new_m_c
  
  def got_new_messages(self):
    return True if self.new_messages_count() > 0 else False
  
  """ Contents Getters """
  def posts(self):
    from application.models import posts
    return posts.Post.all().filter('user =', users.User(email=self.email)).fetch(1000)
  
  def pages(self):
    from application.models import pages
    return pages.Page.all().filter('user =', users.User(email=self.email)).fetch(1000)
  
  def media(self):
    from application.models import media
    return media.Media.all().filter('user =', users.User(email=self.email)).fetch(1000)
  
  def files(self):
    from application.models import files
    return files.File.all().filter('user =', users.User(email=self.email)).fetch(1000)
  
  def links(self):
    from application.models import links
    return links.Link.all().filter('user =', users.User(email=self.email)).fetch(1000)
  
  def contacts(self):
    from application.models import contacts
    return contacts.Contact.all().filter('user =', users.User(email=self.email)).fetch(1000)
  
  def messages(self):
    from application.models import mailbox
    if self.site_messages:
      ms = mailbox.Message.all().fetch(1000)
      ret = []
      for m in ms:
        if str(self.key()) in m.destinations or not m.destinations:
          ret.append(m)
    else:
      ret = mailbox.Message.all().filter('destinations IN', [str(self.key())]).fetch(1000)
    
    return ret