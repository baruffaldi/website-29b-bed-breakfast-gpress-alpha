#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#	GPress - GAE WebSite CMS
#	Copyright (C) 2009 Filippo Baruffaldi
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License along
#	with this program; if not, write to the Free Software Foundation, Inc.,
#	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""
@author: Filippo Baruffaldi <filippo@baruffaldi.info>
@organization: Coolminds Developer Community
@project: GPress
@copyright: Filippo Baruffaldi
@license: GNUv2 License
"""

no_html_fields = [
								  'title',
								  'permalink'
								  ]

skip_fields = [
							 'stream',
							 'key'#,
#							 'sitemap_shared',
#							 'sitemap_priority',
#							 'sitemap_freq',
#							 'feeds_shared',
#							 'contributor_keys',
#							 'protection'
							 ]

import os

from urllib import urlencode

from application.gpress import tools

from datetime import datetime
from application.gpress import models
from google.appengine.ext import db

class File(models.SerializableModel):
	""" Timelines """
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update_date = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	
	""" Content Info """
	permalink = db.StringProperty()
	title = db.StringProperty()
	description = db.TextProperty(indexed=False)
	tags = db.StringListProperty(default=[])
	categories_keys = db.StringListProperty(default=[])

	""" Sharing """
	publish_status = db.IntegerProperty(default=-1)
	feeds_shared = db.BooleanProperty(default=True)
	sitemap_shared = db.BooleanProperty(default=True)
	sitemap_priority = db.FloatProperty()
	sitemap_freq = db.StringProperty()
	protection = db.IntegerProperty(default=0)
	contributors_keys = db.StringListProperty(default=[])
	def contributors(self):
		ret = []
		from application.models.users import User
		for user in self.contributors_keys:
			ret.append(User.get_by_key_name(user))
		return ret

	""" Content Details """	
	main = db.SelfReferenceProperty()
	filename = db.StringProperty()
	mimetype = db.StringProperty(default="multipart/form-data")
	size = db.IntegerProperty(default=0)
	stream = db.BlobProperty()	
	counter = db.IntegerProperty(default=0)
	language = db.StringProperty()
		
	""" Utils """
	
	def languages_count(self):
		return File.all().filter('permalink =', self.permalink).filter('language !=', self.language).count(1000)
	
	def languages(self):
		return File.all().filter('permalink =', self.permalink).filter('language !=', self.language).fetch(1000)
	
	def update_sharing(self, values_dict={}):
		self.contributors_keys = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "contributors_keys" and value:
				self.contributors_keys.extend((",".join([str(x).strip() for x in value])).split(","))
				#self.contributors_keys.append(str(value))
				#self.contributors_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.contributors_keys.count(""):
					#self.contributors_keys.remove("")
				while self.contributors_keys.count(""):
					self.contributors_keys.remove("")
		
	def update(self, values_dict={}):
		self.categories_keys = []
		self.contributors_keys = []
		self.tags = []
		for name in values_dict:
			value = str(values_dict[name]).strip() if name in no_html_fields else values_dict.getall(name)
			if isinstance(value, list) and len(value) == 1 and name[:4] != 'keys':
				value = value[0]
			if name == "feeds_shared":
					self.feeds_shared = bool(int(value))
			elif name == "sitemap_shared":
				self.sitemap_shared = bool(int(value))
			elif name == "sitemap_priority":
				self.sitemap_priority = float(value)
			elif name == "protection":
				self.protection = int(value)
			elif name == "publish_status":
				self.publish_status = int(value)
			elif name == "date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "update_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.update_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "publish_date":
				date = value.split(' ')
				year, month, day = date[0].split('-')
				hours, minutes, seconds = date[1].split(':')
				self.publish_date = datetime(int(year), int(month), int(day), int(hours), int(minutes), int(seconds.split('.')[0]))
			elif name == "filename":
				self.filename = "%s.%s" % ( value, self.extension() )
			elif name == "permalink":
				self.permalink = tools.key_name(str(value).strip())
			elif name == "categories_keys" and value:
				self.categories_keys.extend((",".join([str(x) for x in value])).split(","))
				#self.categories_keys.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				#while self.categories_keys.count(""):
					#self.categories_keys.remove("")
			elif name == "tags":
				self.tags.extend(",".join([x.strip() if x.strip()  else "" for x in value.split(',')]).split(','))
				while self.tags.count(""):
					self.tags.remove("")
			elif name == "new_category" and value:
				cat_check = FileCategory.all(keys_only=True).filter('title =', str(value).strip())
				if cat_check.count(1) and str(cat_check.fetch(1)[0]) not in self.categories_keys:
					self.categories_keys.append(str(cat_check.fetch(1)[0]))
				else:
					cat = FileCategory(title=str(value).strip())
					cat.put()
					if cat.is_saved() and str(cat.key()) not in self.categories_keys:
						self.categories_keys.append(str(cat.key()))
						
			elif name not in skip_fields and name in self.properties().keys():
				if isinstance(self.__getattribute__(name), db.IntegerProperty):
					self.__setattr__(name, int(str(value).strip()))
				elif isinstance(self.__getattribute__(name), db.FloatProperty):
					self.__setattr__(name, float(str(value).strip()))
				else:
					self.__setattr__(name, str(value).strip())
					
	def get_user(self):
		from application.models import users
		return users.User.get_by_key_name(self.user)
	
	def get_update_user(self):
		from application.models import users
		return users.User.get_by_key_name(self.update_user)
		
	def get_kind(self):
		return 'Files'
		
	def categories(self):
		return FileCategory.get(",".join([db.Key(x) if x else db.Key() for x in self.categories_keys]).split(','))
				
	def categories_list(self):
		return db.get(self.categories_keys)
				
	def view_url(self):
		return '/files/%s' % self.permalink
				
	def update_url(self):
		return '/gp-admin/files/%s' % self.key()
				
	def download_url(self):
		return '/files/%s/download' % self.key()
				
	def filename_without_extension(self):
		if self.filename:
			return ".".join([ x for x in str(self.filename).split('.')[:1]])
				
	def extension(self):
		if self.filename:
			return str(self.filename).split('.')[-1]
	
	def thumbnail_url(self):
		if self.mime() in ['image']:
			return "/files/%s/download" % str(self.permalink)
		
		return False
	
	def mime(self):
		if self.mimetype is not None:
			return str(self.mimetype).split('/')[0]
	
	def type(self):
		if self.mimetype is not None:
			return str(self.mimetype).split('/')[1]
	
	def mimetype_image(self):
		return "/gp-admin/images/icons/mimetype/gpress-icon-mimetype-%s.png" % str(self.mimetype).split("/")[0]
	
	def mimetype_image_url(self):
		return "/gp-admin/images/icons/mimetype/gpress-icon-mimetype-%s.png" % str(self.mimetype).split("/")[0]	
	
	def comments(self):
		return FileComment.all().filter('content =', self.key()).fetch(1000)
	
	def comments_counter(self):
		return FileComment.all().filter('content =', self.key()).count(1000)
	
	def url(self):
		return {'download': '/files/%s/download'	% self.key()}
	
	def code(self, width=200):
		if self.mime() in ['image']:
			thumb_url = '/media/%s/download?width=%i&amp;disable_counter=1' % ( self.image.permalink, width )
		else:
			thumb_url = 'http://api.thumbalizr.com/?%s&amp;width=%s' % (urlencode({'url': 'http://%s/media/%s/download?width=%i&amp;disable_counter=1' % (os.environ["HTTP_HOST"], self.permalink, width)}), width)
		return "<img src='%s' alt='Preview Image' />" % thumb_url

class FileCategory(db.Model):
	user = db.UserProperty(auto_current_user_add=True)
	date = db.DateTimeProperty(auto_now_add=True)
	update = db.DateTimeProperty(auto_now=True)
	update_user = db.UserProperty(auto_current_user=True, auto_current_user_add=True)
	
	title = db.CategoryProperty()
	
	def contents(self):
		return File.all().filter('categories_keys IN', [str(self.key())]).fetch(1000)
	
	def contents_count(self):
		return File.all().filter('categories_keys IN', [str(self.key())]).count(1000)

class FileComment(db.Model):
		user = db.UserProperty(auto_current_user_add=True)
		date = db.DateTimeProperty(auto_now_add=True)
		
		content = db.ReferenceProperty(File)
		body = db.TextProperty(indexed=False)

		status = db.IntegerProperty(default=-1)