import os

USE_I18N = True

LANGUAGES = (
    ('en', _('English')),
    ('it', _('Italiano'))
)

LANGUAGE_CODE = 'en'

COMPRESS = not os.environ["SERVER_SOFTWARE"].split("/")[0].lower() == "development"
COMPRESS_AUTO = COMPRESS
COMPRESS_VERSION = False
COMPRESS_VERSION_PLACEHOLDER = '?'
COMPRESS_VERSION_DEFAULT = '0'

COMPRESS_CSS_FILTERS = ['application.filters.csstidy.CSSTidyFilter']
COMPRESS_JS_FILTERS = ['application.filters.jsmin.JSMinFilter']
COMPRESS_CSS = {}
COMPRESS_JS = {}

if COMPRESS_CSS_FILTERS is None:
    COMPRESS_CSS_FILTERS = []

if COMPRESS_JS_FILTERS is None:
    COMPRESS_JS_FILTERS = []

DEBUG = os.environ["SERVER_SOFTWARE"].split("/")[0].lower() == "development"


LOCALE_PATHS = ['../../../locale']
#TEMPLATE_DIRS = (
    
#)
#INSTALLED_APPS = ("../../../application")